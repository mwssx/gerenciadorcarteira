<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form:form action="${s:mvcUrl('TC#gravar').build()}" method="post"
		commandName="transacao">
		<div>
			<label>Data da Realização Transação</label>
			<form:input path="dataRealizacao" />
		</div>
		<div>
			<label>Valor Transacao</label>
			<form:input path="valorTotalTransacao" />
		</div>
		<div>
			<label>Quantidade de Ativos Transacao</label>
			<form:input path="quantidade" />
		</div>
		<div>
			<label>Valor Taxa Transacao</label>
			<form:input path="taxaTransacao" />
		</div>
		
		<div>
		<label>Tipo Transacao</label>
		  <form:select  path="tipo">
		    <form:option value="NONE"> --SELECT--</form:option>
		    <form:options items="${transacaoTipo}"></form:options>
		  </form:select>
		</div>
		
		<div>
		<label>Ação</label>
		  <form:select  path="acao">
		    <form:option value="NONE"> --SELECT--</form:option>
		    <form:options items="${acoes}"></form:options>
		  </form:select>
		</div>
		
		<button type="submit">Cadastrar</button>
	</form:form>
</body>
</html>