<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Lista de A��es</h1>
	<p>${sucesso}</p>
	<p>${falha}</p>
	<table border="1">
		<tr>
			<td>Codigo</td>
			<td>Nome</td>
			<td>Valor</td>
			<td>Opera��es</td>
		</tr>
		<c:forEach items="${acoes}" var="acao">
			<tr>
				<td>${acao.codigo}</td>
				<td>${acao.nome}</td>
				<td>${acao.valor}</td>
				<td>
					<a href="${s:mvcUrl('AC#detalhe').arg(0, acao.id).build()}"> Alterar</a> / Excluir</a> /
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>