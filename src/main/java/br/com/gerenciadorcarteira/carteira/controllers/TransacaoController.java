package br.com.gerenciadorcarteira.carteira.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.gerenciadorcarteira.carteira.dao.AcaoDAO;
import br.com.gerenciadorcarteira.carteira.dao.TransacaoDAO;
import br.com.gerenciadorcarteira.carteira.modelos.Acao;
import br.com.gerenciadorcarteira.carteira.modelos.Transacao;
import br.com.gerenciadorcarteira.carteira.modelos.TransacaoTipo;

@Controller
@RequestMapping("/transacao")
public class TransacaoController {
	
	@Autowired
	private AcaoDAO acaoDAO;
	
	@Autowired
	private TransacaoDAO transacaoDAO;


	@RequestMapping("/form")
	public ModelAndView form(Transacao acao) {
		List<Acao> acoes = acaoDAO.listar();
		Map<String, String> mapAcoes = new HashMap<String, String>();
		
		for (Acao acao2 : acoes) {
			mapAcoes.put(acao2.getId().toString(), acao2.getCodigo().toString());
		}
		
		ModelAndView modelAndView = new ModelAndView("transacao/form");
		modelAndView.addObject("tipos", TransacaoTipo.values());
		modelAndView.addObject("acoes", mapAcoes);
		
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listar() {
		List<Transacao> transacao = transacaoDAO.listar();
		ModelAndView modelAndView = new ModelAndView("transacao/lista");
		modelAndView.addObject("transacao", transacao);
		
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView gravar(@RequestBody Transacao transacao, RedirectAttributes redirectAttributes) {
		transacaoDAO.gravar(transacao);
		redirectAttributes.addFlashAttribute("sucesso", "Acao cadastrada com sucesso!");
		return new ModelAndView("redirect:transacao");
	}
	
	
	@RequestMapping("/{id}")
	@ResponseBody
	public Transacao detalheJson(@PathVariable("id") Long id){
		Transacao transacao = transacaoDAO.find(id);
		transacao.setAcao(null);
	    return transacao;
	}

}
