package br.com.gerenciadorcarteira.carteira.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.gerenciadorcarteira.carteira.dao.UsuarioDAO;
import br.com.gerenciadorcarteira.carteira.modelos.*;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioDAO usuarioDAO;


	@RequestMapping("/form")
	public ModelAndView form(Usuario usuario) {
		ModelAndView modelAndView = new ModelAndView("usuarios/form");
		//modelAndView.addObject("tipos", TipoPreco.values());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listar() {
		List<Usuario> usuarios = usuarioDAO.listar();
		ModelAndView modelAndView = new ModelAndView("usuarios/lista");
		modelAndView.addObject("usuarios", usuarios);
		
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView gravar(Usuario usuario, RedirectAttributes redirectAttributes) {
		usuarioDAO.gravar(usuario);
		redirectAttributes.addFlashAttribute("sucesso", "Usuario cadastrada com sucesso!");
		return new ModelAndView("redirect:usuarios");
	}
	
	@RequestMapping("/detalhe/{id}")
	public ModelAndView detalhe(@PathVariable("id") Long id){
	    ModelAndView modelAndView = new ModelAndView("/usuarios/detalhe");
	    Usuario usuario = usuarioDAO.find(id);   
	    modelAndView.addObject("usuario", usuario);
	    return modelAndView;
	}
	
	@RequestMapping("/{id}")
	@ResponseBody
	public Usuario detalheJson(@PathVariable("id") Long id){
		Usuario produto = usuarioDAO.find(id);   
	    return produto;
	}
}
