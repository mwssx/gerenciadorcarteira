package br.com.gerenciadorcarteira.carteira.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.gerenciadorcarteira.carteira.dao.AcaoDAO;
import br.com.gerenciadorcarteira.carteira.modelos.Acao;

@Controller
@RequestMapping("/acoes")
public class AcaoController {
	
	@Autowired
	private AcaoDAO acaoDAO;


	@RequestMapping("/form")
	public ModelAndView form(Acao acao) {
		ModelAndView modelAndView = new ModelAndView("acoes/form");
		//modelAndView.addObject("tipos", TipoPreco.values());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listar() {
		List<Acao> acoes = acaoDAO.listar();
		ModelAndView modelAndView = new ModelAndView("acoes/lista");
		modelAndView.addObject("acoes", acoes);
		
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView gravar(Acao acao, RedirectAttributes redirectAttributes) {
		acaoDAO.gravar(acao);
		redirectAttributes.addFlashAttribute("sucesso", "Acao cadastrada com sucesso!");
		return new ModelAndView("redirect:acoes");
	}
	
	@RequestMapping("/detalhe/{id}")
	public ModelAndView detalhe(@PathVariable("id") Long id){
	    ModelAndView modelAndView = new ModelAndView("/acoes/detalhe");
	    Acao acao = acaoDAO.find(id);   
	    modelAndView.addObject("acao", acao);
	    return modelAndView;
	}
	
	@RequestMapping("/{id}")
	@ResponseBody
	public Acao detalheJson(@PathVariable("id") Long id){
		Acao acao = acaoDAO.find(id);   
		acao.setTransacao(null);
	    return acao;
	}

}
