package br.com.gerenciadorcarteira.carteira.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.gerenciadorcarteira.carteira.modelos.*;


@Repository
@Transactional
public class UsuarioDAO {

	@PersistenceContext
    private EntityManager manager;// gerenciador de entidades fernecido pelo Spring, quem manipula as entidades no DAO
	
	public void gravar(Usuario usuario){
		manager.merge(usuario);
    }
	
	public void excluir(Usuario usuario){
		manager.remove(usuario);
	}

	public List<Usuario> listar() {
		return manager.createQuery("select a from Usuario a", Usuario.class).getResultList();
	}
	
	public Usuario find(Long id){
		return manager.createQuery("select a from Usuario a "
				+ "where a.id = :id", Usuario.class)
				.setParameter("id", id)
				.getSingleResult();
	}
	
	public Usuario findByCodigoUsuario(String codigo){
		return manager.createQuery("select a from Usuario a "
				+ "where a.codigo = :codigo", Usuario.class)
				.setParameter("codigo", codigo)
				.getSingleResult();
	}
}
