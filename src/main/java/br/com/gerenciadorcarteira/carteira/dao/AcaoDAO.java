package br.com.gerenciadorcarteira.carteira.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.gerenciadorcarteira.carteira.modelos.Acao;


@Repository
@Transactional
public class AcaoDAO {
	
	@PersistenceContext
    private EntityManager manager;// gerenciador de entidades fernecido pelo Spring, quem manipula as entidades no DAO
	
	public void gravar(Acao acao){
		manager.merge(acao);
    }
	
	public void excluir(Acao acao){
		manager.remove(acao);
	}

	public List<Acao> listar() {
		return manager.createQuery("select a from Acao a", Acao.class).getResultList();
	}
	
	public Acao find(Long id){
		return manager.createQuery("select a from Acao a "
				+ "where a.id = :id", Acao.class)
				.setParameter("id", id)
				.getSingleResult();
	}
	
	public Acao findByCodigoAcao(String codigo){
		return manager.createQuery("select a from Acao a "
				+ "where a.codigo = :codigo", Acao.class)
				.setParameter("codigo", codigo)
				.getSingleResult();
	}

}
