package br.com.gerenciadorcarteira.carteira.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.gerenciadorcarteira.carteira.modelos.Provento;


@Repository
@Transactional
public class ProventoDAO {
	
	@PersistenceContext
    private EntityManager manager;
	
	public void gravar(Provento Provento){
		manager.merge(Provento);
    }
	
	public void excluir(Provento Provento){
		manager.remove(Provento);
	}

	public List<Provento> listar() {
		return manager.createQuery("select a from Provento a", Provento.class).getResultList();
	}
	
	public Provento find(Long idProvento){
		return manager.createQuery("select a from Provento a "
				+ "where a.id = :id", Provento.class)
				.setParameter("idProvento", idProvento)
				.getSingleResult();
	}
	
	public Provento findByCodigoProvento(String codigo){
		return manager.createQuery("select a from Provento a "
				+ "where a.codigo = :codigo", Provento.class)
				.setParameter("codigo", codigo)
				.getSingleResult();
	}

}
