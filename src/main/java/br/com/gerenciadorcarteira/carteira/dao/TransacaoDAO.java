package br.com.gerenciadorcarteira.carteira.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.gerenciadorcarteira.carteira.modelos.Acao;
import br.com.gerenciadorcarteira.carteira.modelos.Transacao;


@Repository
@Transactional
public class TransacaoDAO {
	
	@PersistenceContext
    private EntityManager manager;
	
	public void gravar(Transacao transacao){
		manager.merge(transacao);
    }
	
	public void excluir(Transacao transacao){
		manager.remove(transacao);
	}

	public List<Transacao> listar() {
		return manager.createQuery("select t from Transacao t", Transacao.class).getResultList();
	}
	
	public Transacao find(Long id){
		return manager.createQuery("select t from Transacao t "
				+ "where t.id = :id", Transacao.class)
				.setParameter("id", id)
				.getSingleResult();
	}
	

}
