package br.com.gerenciadorcarteira.carteira.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
public class JPAConfiguration {
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        factoryBean.setJpaVendorAdapter(vendorAdapter);

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUsername("root");
        dataSource.setPassword("");
        dataSource.setUrl("jdbc:mysql://localhost:3306/gerenciadorcarteira");
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");

        factoryBean.setDataSource(dataSource);

        Properties props = new Properties();
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");//define o dialte padrão de comunicação com o banco.
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.hbm2ddl.auto", "update");//sempre que ocorrer alteração na estrutura do banco, o hibernate irá gerencia-lo alterando-o.

        factoryBean.setJpaProperties(props);

        factoryBean.setPackagesToScan("br.com.gerenciadorcarteira.carteira.modelos");// define onde está setada as entitys

        return factoryBean;
    }

	
	@Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf){
		/*
		 * como podemos observar este metodo recebe EntityManager como parametro, logo nas classes onde
		 * é implementado os entityManager deverá ser anotada com transacional
		 */
        return new JpaTransactionManager(emf);
    }

}
