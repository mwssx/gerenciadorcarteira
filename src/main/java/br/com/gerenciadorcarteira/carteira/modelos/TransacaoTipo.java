package br.com.gerenciadorcarteira.carteira.modelos;

public enum TransacaoTipo {
	 COMPRA, VENDA, DIVIDENDOS, JSCP, BONIFICACAO, ALUGUEL;
}
