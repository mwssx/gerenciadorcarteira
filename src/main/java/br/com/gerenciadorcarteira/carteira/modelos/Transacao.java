package br.com.gerenciadorcarteira.carteira.modelos;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Transacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@DateTimeFormat
	private Calendar dataRealizacao;

	private BigDecimal valorTotalTransacao;

	private int quantidade;

	private BigDecimal taxaTransacao;

	private TransacaoTipo tipo;

	@ManyToOne
	@JoinColumn(name = "acao_id")
	private Acao acao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Acao getAcao() {
		return acao;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}

	public Calendar getDataRealizacao() {
		return dataRealizacao;
	}

	public void setDataRealizacao(Calendar dataRealizacao) {
		this.dataRealizacao = dataRealizacao;
	}

	public BigDecimal getValorTotalTransacao() {
		return valorTotalTransacao;
	}

	public void setValorTotalTransacao(BigDecimal valorTotalTransacao) {
		this.valorTotalTransacao = valorTotalTransacao;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getTaxaTransacao() {
		return taxaTransacao;
	}

	public void setTaxaTransacao(BigDecimal taxaTransacao) {
		this.taxaTransacao = taxaTransacao;
	}

	public TransacaoTipo getTipo() {
		return tipo;
	}

	public void setTipo(TransacaoTipo tipo) {
		this.tipo = tipo;
	}

}
