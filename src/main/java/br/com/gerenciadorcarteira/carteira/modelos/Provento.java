package br.com.gerenciadorcarteira.carteira.modelos;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Provento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProvento;
	private Long idAcao;
	private String codigo;
	private String nome;
	private BigDecimal valorAnterior;
	private BigDecimal valorCompra;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataCompra;
	
	public Long getIdProvento() {
		return idProvento;
	}
	public void setIdProvento(Long idProvento) {
		this.idProvento = idProvento;
	}
	
	public Long getIdAcao() {
		return idAcao;
	}
	public void setIdAcao(Long idAcao) {
		this.idAcao = idAcao;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BigDecimal getValorAnterior() {
		return valorAnterior;
	}
	public void setValorAnterior(BigDecimal valorAnterior) {
		this.valorAnterior = valorAnterior;
	}
	public BigDecimal getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}
	public Date getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	
}


	